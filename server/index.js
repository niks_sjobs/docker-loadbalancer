const app = require("express")();
const appName = process.env.NAME;

app.get("/", (req,res) => 
res.send(`Hello from : ${appName} home page`))
 
app.listen(80, ()=>console.log(`${appName} is listening on 80`))