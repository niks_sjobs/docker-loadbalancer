#!/bin/sh

CONTAINER_NAME="nginxapp_container"
IMAGE_NAME="nginxapp_image"

docker rm -f $CONTAINER_NAME
docker image rm $IMAGE_NAME
docker build -t $IMAGE_NAME .
docker run -p 80:8080 -d --name $CONTAINER_NAME $IMAGE_NAME