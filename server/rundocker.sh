#!/bin/sh

CONTAINER_NAME1="nodeapp_container-1111"
CONTAINER_NAME2="nodeapp_container-2222"
CONTAINER_NAME3="nodeapp_container-3333"
CONTAINER_NAME4="nodeapp_container-4444"
IMAGE_NAME="nodeapp_image"

# Delete all exisiting containers
docker rm -f $CONTAINER_NAME1
docker rm -f $CONTAINER_NAME2
docker rm -f $CONTAINER_NAME3
docker rm -f $CONTAINER_NAME4

# Delete all exisiting images
docker image rm $IMAGE_NAME

# Build new image
docker build -t $IMAGE_NAME .

# Build new container
docker run -d -e APPID=1111 --name $CONTAINER_NAME1 $IMAGE_NAME
docker run -d -e APPID=2222 --name $CONTAINER_NAME2 $IMAGE_NAME
docker run -d -e APPID=3333 --name $CONTAINER_NAME3 $IMAGE_NAME
docker run -d -e APPID=4444 --name $CONTAINER_NAME4 $IMAGE_NAME