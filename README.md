**Prerequisite**

Docker

Run following commands

```
cd <path/to/project>
docker-compose down --rmi all
docker-compose up -d --build
```

Then visit `http://localhost:80`, every time you refresh page, you will get hie from different servers behind lb. (In round robin fashion)
